package com.springtutes.crudassmt.Service;

import java.util.List;

import com.springtutes.crudassmt.Model.User;
import com.springtutes.crudassmt.Repository.JPARepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CRUDService {

    @Autowired
    private JPARepo repo;

    public List<User> getUserList(){
        return repo.findAll();
    }

    public String addUser(User user){
        try {
            repo.save(user);
            return "Success";
        } catch (Exception e) {
            return "Error";
        }
    }

    public boolean isEmailExist(String str){
        List<User> lst = this.getUserList(); 
        boolean status = false;
        for(User user : lst){
            if(user.getEmail().equals(str)){
                status = true;
            }
        }
        return status;
    }

    public User getById(Integer id){
       return repo.findById(id).get();
    }

    public String updateUser(User user, Integer id){
        String str = "";
        try {
            User existingUser = this.getById(id);

           
            try {
                if(!user.getFirstname().isBlank() || user.getFirstname() != null){
                    existingUser.setFirstname(user.getFirstname());
                }
            } catch (Exception e) {
            
            }

            try {
                if(!user.getLastname().isBlank() || user.getLastname() != null){
                    existingUser.setLastname(user.getLastname());
                }
            } catch (Exception e) {
                //TODO: handle exception
            }

            try {
                if(!user.getAddress().isBlank() || user.getAddress() != null){
                    existingUser.setAddress(user.getAddress());
                }
              
            } catch (Exception e) {
                //TODO: handle exception
            }
           
            try {
                if(!user.getDob().toString().isBlank() || user.getDob() != null){
                    existingUser.setDob(user.getDob());
                }
            } catch (Exception e) {
                //TODO: handle exception
            }

            try {
                if(!user.getEmail().isBlank() || user.getEmail() != null){
                    existingUser.setEmail(user.getEmail());
                }
                
            } catch (Exception e) {
                //TODO: handle exception
            }

            try {
                if(!user.getGender().isBlank() || user.getGender() != null){
                    existingUser.setGender(user.getGender());
                }
            } catch (Exception e) {
                //TODO: handle exception
            }

            try {
                if(!user.getNicno().isBlank() || user.getNicno() != null){
                    existingUser.setNicno(user.getNicno());
                }
               
            } catch (Exception e) {
                //TODO: handle exception
            }

            try {
                if(!user.getPhoneno().isBlank() || user.getPhoneno() != null){
                    existingUser.setPhoneno(user.getPhoneno());
                }
            } catch (Exception e) {
                //TODO: handle exception
            }

            str = "User Updated";

        } catch (Exception e) {
            str = "No user available";
        }
        return str;
    }

    public String deleteUser(int id){
        String str;
        try {
            repo.delete(this.getById(id));
            str = "Succesfully deleted.";
        } catch (Exception e) {
            str = "No such user.";
        }
        return str;
    }

    public String userLogin(String uname, String pword){
        List<User> lst = this.getUserList();
        String str = "";
        boolean unamest = false;
        boolean pwst = false;
        for(User user : lst){
            if(user.getFirstname().equals(uname)){
                unamest = true;
            }
            if(user.getPassword().equals(pword)){
                pwst = true;
            }
        }

        if(!unamest || !pwst){
            str = "Username or password incorrect.";
        }
        else{
            str = "Login Success.";
        }

        return str;
    }
}