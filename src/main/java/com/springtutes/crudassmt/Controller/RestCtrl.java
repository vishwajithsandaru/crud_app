package com.springtutes.crudassmt.Controller;

import java.util.List;
import com.springtutes.crudassmt.Model.User;
import com.springtutes.crudassmt.Service.CRUDService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;



@RestController
public class RestCtrl {

    @Autowired
    private CRUDService crservice;

    @GetMapping(value="/listall")
    public List<User> listAll(){
        return crservice.getUserList();
    }

    @PostMapping("/add")
    public String addUser(@RequestBody User user){
        String str = "";
        if(!crservice.isEmailExist(user.getEmail())){
           if(user.getFirstname() != null && user.getEmail() != null && user.getPassword() != null){
               str = crservice.addUser(user);
           }else{
               str = "Fields cannot be empty.";
           }
        }
        else{
            str = "This email already exists.";
        }
        return str;
        
    }

    // updating data through: PUT
    @PutMapping(value="/updateuser/{id}")
    public String updateUser(@RequestBody User user, @PathVariable Integer id) {
        String str = crservice.updateUser(user, id);
        return str;
    }

    // delete data: POST
    @PostMapping(value="/delete/{id}")
    public String deleteUser(@PathVariable Integer id) {
        String str = crservice.deleteUser(id);
        return str;
    }
    
    // login
    @GetMapping(value = "/login")
    public String loginFunc(@RequestParam String username, @RequestParam String password){
        String str = crservice.userLogin(username, password);
        return str;
    }
    

}