package com.springtutes.crudassmt.Repository;
import com.springtutes.crudassmt.Model.User;

import org.springframework.data.jpa.repository.JpaRepository;

public interface JPARepo extends JpaRepository<User, Integer>{
    
}